This file is for taking notes as you go.

1. After reading the README.md file several times, I created a GitLab account and forked the two-am-project.
2. I installed the requered versions of Node and Go in my environment setup.
3. I am starting up the project with the provided commands 
    a. Frontend
        1. npm install 
        2. npm run serve
    b.  Backend
        1. go mod download 
        2. go run ./server -v
        3. go test ./server -v
4. With the information provided, what I will look into is after running the tests  which message (error, exception) it throws in the test that is failing. (this is what I know as reproducing the error).
5. After recreating the error I noticed that the following error (exception) was thrown, so I will research about it and look into the test codethat is failing.
    a.  main_test.go:55: 
                Error Trace:    main_test.go:55
                Error:          "{\"OK\":true}\n" does not contain "\"ok\":true"
                Test:           TestServerSuite/TestHealthCheck
6. The problem with the failing test (TestServerSuite/TestHealthCheck) is that the assert is evaluating that a response string contains an expected string but the expected string provided has the ¨ok¨ and the response string has that word in uppercase (¨OK¨). 
    a. In the handlers.go file I change the OK property to ok in order to pass the test and be align with the frontend health check method.
7. The test named: TestWebsockets PASS but the assert is evaluating a string message instead of a JSON that contains the message and other properties of the message that the frontend will manage.
    a. In this case, we will need to put this message in a JSON format and assert the message but within the JSON.
    This was my primary idea, but I need more research on GO lang so I can complete the change. Then, I used my remaining time to the frontend task that  I can handle it faster.
8. In the frontend we will need to create a new text box for the name field and create the labels to display each message name with the text
    a. Now the user can send a name with the message and it will be displayed but I don´t have the time to position the name where I want (above the message).